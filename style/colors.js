const background = '#FFFFFF';
const primary = '#219ebcFF';
const secondary = '#219ebcBB';
const accent = '#1ba9cb';
const green = '#23982e';
const transparentGreen = '#FFFFFF80';

export const colors = {
    'black': '#000000',
    'gray': '#7c7c7c',
    'background': background,
    'textPrimary': primary,
    'textSecondary': secondary,
    'accent': accent,
    'borderColor': background,
    'priceBgColor': green,
    'priceBgColorLighten': transparentGreen,
    'priceTextColor': '#FFFFFF',

    'buttonBgColor': primary,
    'buttonTextColor': background,
    'buttonOutlineBgColor': background,
    'buttonOutlineTextColor': '#a21c1c',
    'buttonOutlineBorderColor': primary,
};
