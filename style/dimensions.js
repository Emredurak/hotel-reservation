export const padding = {
    xShort: 4,
    short: 8,
    standard: 16,
    big: 24,
};

export const margin = {
    xShort: 4,
    short: 8,
    standard: 16,
    big: 24,
};

export const border = {
    bigRadius: 30,
    imageRadius: 10,
    thin: 1,
};

export const font = {
    short: 12,
    standard: 16,
    big: 20,
};

export const icon = {
    size: 20,
}
