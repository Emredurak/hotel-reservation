import React, {useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {showLocation} from 'react-native-map-link'
import {
    ActivityIndicator,
    Animated,
    InteractionManager,
    Modal,
    Pressable,
    ScrollView,
    StyleSheet,
    Text,
    TouchableNativeFeedback,
    View,
} from 'react-native';
import {border, font, icon, margin, padding} from "../../style/dimensions";
import {colors} from "../../style/colors";
import {Ionicons} from "@expo/vector-icons";
import {fonts} from "../../style/variables";
import {strings} from "../../style/strings";
import {Rating} from "react-native-ratings";
import getSymbolFromCurrency from "currency-symbol-map";
import Gallery from "../components/Gallery";
import InfoElement from "../components/InfoElement";
import {deletePost, savePost} from "../services/savedPosts/action";


function Detail(props) {
    const [interactionsComplete, setInteractionsComplete] = useState(false);
    useEffect(() => {
        InteractionManager.runAfterInteractions(() => {
            setInteractionsComplete(true);
        });
    }, []);
    const savedPosts = useSelector((state) => state.savedPosts.savedPosts);
    const dispatch = useDispatch();
    const hotelInfo = props.route.params;
    let isExist = false;
    for (let i = 0; i < savedPosts.length; i++) {
        let item = savedPosts[i];
        if (item.id === hotelInfo.id) {
            isExist = true;
            break;
        }
    }
    const [modalVisible, setModalVisible] = useState(false);
    const [bookmarked, setBookmark] = useState(isExist);
    const bookmarkAnimation = useRef(new Animated.Value(1)).current;
    const startBookmarkAnimation = () => {
        if (!bookmarked) {
            saveToRedux();
        } else {
            deleteFromRedux();
        }
        Animated.timing(bookmarkAnimation, {
            toValue: 0.1,
            duration: 1,
            useNativeDriver: true,
        }).start(() => {
            /* completion callback */
            setBookmark(!bookmarked);
            finishBookmarkAnimation();
        });
    };
    const finishBookmarkAnimation = () => {
        Animated.timing(bookmarkAnimation, {
            toValue: 1,
            duration: 250,
            useNativeDriver: true,
        }).start(() => {
            /* completion callback */
            saveToRedux();
        });
    };
    const saveToRedux = () => {
        dispatch(savePost(hotelInfo));
    };
    const deleteFromRedux = () => {
        dispatch(deletePost(hotelInfo));
    };
    const openMaps = () => {
        showLocation({
            latitude: hotelInfo.location.latitude,
            longitude: hotelInfo.location.longitude,
            title: hotelInfo.name,  // optional
        })
    }

    const book = () => {
        setModalVisible(true);
    }

    if (!interactionsComplete) {
        return (
            <ActivityIndicator
                style={styles.activityIndicator}
                size={'large'}
                color={colors.accent}
            />
        );
    }

    return (
        <View style={styles.container}>
            <ScrollView style={styles.scrollView}>
                <Gallery imageSource={hotelInfo.gallery}/>
                <TouchableNativeFeedback style={{zIndex: 10}} onPress={startBookmarkAnimation}>
                    <Animated.View
                        style={[
                            styles.saveIconWrapper,
                            {transform: [{scale: bookmarkAnimation}]},
                        ]}>
                        {bookmarked ? <Ionicons
                            name={"md-heart"}
                            style={styles.saveIcon}
                            size={20}/> : <Ionicons
                            name={"md-heart-outline"}
                            style={styles.saveIcon}
                            size={20}/>

                        }
                    </Animated.View>
                </TouchableNativeFeedback>
                <View style={styles.detailsContainer}>
                    <View style={styles.headerWrapper}>
                        <View style={styles.ratingWrapper}>
                            <Rating
                                style={styles.rating}
                                type="custom"
                                readonly
                                startingValue={hotelInfo.stars}
                                ratingBackgroundColor='#f1c40f30'
                                imageSize={icon.size}/>
                            <Ionicons
                                name={"md-ellipse"}
                                style={styles.dotSeparator}
                                size={4}/>
                            <Text style={styles.starDesc}>{hotelInfo.stars} Star Hotel</Text>
                        </View>
                        <Text style={styles.name}>{hotelInfo.name}</Text>
                        <View style={styles.ratingWrapper}>
                            <Text style={styles.ratingText}>{hotelInfo.userRating}</Text>
                            <Text style={styles.price}>
                                {hotelInfo.price} {getSymbolFromCurrency(hotelInfo.currency)}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.body}>
                        <InfoElement
                            label={strings.description}
                            iconName={"md-information-circle-outline"}
                            text={strings.descriptionPlaceHolder}/>
                        <InfoElement
                            label={strings.checkInOut}
                            iconName={"md-checkmark-done-outline"}>
                            <View style={styles.checkInOut}>
                                <Ionicons
                                    name={"md-log-in-outline"}
                                    style={styles.icon}
                                    size={icon.size}/>
                                <Text>{hotelInfo.checkIn.from} - {hotelInfo.checkIn.to}</Text>
                            </View>
                            <View style={styles.checkInOut}>
                                <Ionicons
                                    name={"md-log-out-outline"}
                                    style={styles.icon}
                                    size={icon.size}/>
                                <Text>{hotelInfo.checkOut.from} - {hotelInfo.checkOut.to}</Text>
                            </View>
                        </InfoElement>
                        <InfoElement
                            label={strings.contact}
                            iconName={"md-logo-rss"}
                        >
                            <View style={styles.checkInOut}>
                                <Ionicons
                                    name={"md-mail-outline"}
                                    style={styles.icon}
                                    size={icon.size}/>
                                <Text>{hotelInfo.contact.email}</Text>
                            </View>
                            <View style={styles.checkInOut}>
                                <Ionicons
                                    name={"md-call-outline"}
                                    style={styles.icon}
                                    size={icon.size}/>
                                <Text>{hotelInfo.contact.phoneNumber}</Text>
                            </View>
                        </InfoElement>
                        <InfoElement
                            label={strings.address}
                            iconName={"md-location-outline"}
                            text={hotelInfo.location.address + " / " + hotelInfo.location.city}>
                            <View>
                                <Text>
                                    {hotelInfo.location.address + " / " + hotelInfo.location.city}
                                </Text>
                                <TouchableNativeFeedback onPress={() => openMaps()}>
                                    <Text style={styles.link}>Open in Map</Text>
                                </TouchableNativeFeedback>
                            </View>
                        </InfoElement>
                    </View>
                </View>
                <View style={styles.centeredView}>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => {
                            setModalVisible(!modalVisible);
                        }}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.modalText}>Booking Successful!</Text>
                                <Pressable
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={() => setModalVisible(!modalVisible)}>
                                    <Ionicons name="md-checkmark" size={20} color={colors.background}/>
                                </Pressable>
                            </View>
                        </View>
                    </Modal>
                </View>
            </ScrollView>
            <View style={styles.footer}>
                <TouchableNativeFeedback onPress={() => book()}>
                    <View style={styles.buttonWrapper}>
                        <Text style={styles.actionButton}>Book</Text>
                    </View>
                </TouchableNativeFeedback>
            </View>
        </View>
    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollView: {
        flex: 1,
    },
    saveIconWrapper: {
        height: 40,
        width: 40,
        borderRadius: 20,
        alignSelf: 'flex-end',
        marginTop: margin.short,
        marginBottom: -20,
        marginRight: padding.standard + 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 4,
        zIndex: 10,
    },
    saveIcon: {
        height: icon.size * 2,
        width: icon.size * 2,
        paddingVertical: icon.size / 2,
        borderRadius: icon.size,
        overflow: 'hidden',
        textAlign: 'center',
        textAlignVertical: 'center',
        backgroundColor: colors.buttonOutlineBgColor,
        color: colors.buttonOutlineTextColor,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 4,
        zIndex: 150,
    },
    dotSeparator: {
        color: colors.gray,
        marginHorizontal: margin.standard,
    },
    detailsContainer: {
        flexGrow: 1,
        padding: padding.standard,
        backgroundColor: colors.background,
        borderTopLeftRadius: border.bigRadius,
        borderTopRightRadius: border.bigRadius,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 4,
    },
    headerWrapper: {
        marginTop: margin.standard,
    },
    ratingWrapper: {
        flexDirection: 'row',
        alignItems: 'center',

    },
    starDesc: {
        color: colors.gray,
        fontSize: font.short,
    },
    ratingText: {
        backgroundColor: colors.textPrimary,
        color: colors.background,
        borderRadius: 4,
        fontWeight: 'bold',
        paddingVertical: 2,
        paddingHorizontal: 4,
        marginRight: margin.standard,
    },
    rating: {},
    price: {
        textAlignVertical: 'center',
        textAlign: 'center',
        fontSize: font.standard,
        alignSelf: 'flex-end',
        color: colors.background,
        marginLeft: 'auto',
        paddingHorizontal: padding.standard,
        paddingVertical: padding.xShort,
        backgroundColor: colors.textPrimary,
        borderRadius: border.imageRadius,
        fontWeight: 'bold',
    },
    body: {
        flex: 1,
        paddingVertical: 30,
        backgroundColor: colors.background,
    },
    checkInOut: {
        flex: 1,
        flexDirection: 'row',
        marginVertical: margin.xShort,
    },
    label: {
        paddingLeft: padding.xShort,
        paddingRight: padding.standard,
        color: colors.textPrimary,
        textAlign: 'left',
        textAlignVertical: 'center',
        borderBottomWidth: 1,
        borderColor: colors.textSecondary,

    },
    icon: {
        marginRight: margin.xShort,
        color: colors.accent,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    name: {
        fontFamily: fonts.standardFontFamily,
        fontWeight: 'bold',
        fontSize: font.big,
        marginVertical: margin.xShort,
    },
    link: {
        paddingVertical: padding.short,
        color: 'blue',
        fontWeight: 'bold',
    },
    buttonWrapper: {
        margin: padding.standard,
        backgroundColor: colors.priceBgColor,
        borderRadius: border.imageRadius,
        paddingVertical: padding.short,
        paddingHorizontal: padding.standard,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 4,
    },
    actionButton: {
        textTransform: 'uppercase',
        color: colors.priceTextColor,
        fontSize: font.big,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    footer: {
        backgroundColor: colors.background,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonClose: {
        backgroundColor: colors.priceBgColor,
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    activityIndicator: {
        alignSelf: 'center',
        flex: 1,
    },
});

export default Detail;
