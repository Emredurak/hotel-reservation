import * as React from 'react';
import {useEffect, useState} from 'react';
import {
    ActivityIndicator,
    Keyboard,
    Platform,
    Pressable,
    SafeAreaView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import {CollapsibleHeaderFlatList} from 'react-native-collapsible-header-views';
import {Ionicons} from "@expo/vector-icons";
import {colors} from "../../style/colors";
import FilterItem from "../components/FilterItem";
import {margin, padding} from "../../style/dimensions";
import Hotel from "../components/Hotel";

const Home = ({navigation}) => {
    const renderItem = ({item}) => (
        <Hotel
            navigation={navigation}
            id={item.id}
            name={item.name}
            location={item.location}
            stars={item.stars}
            checkIn={item.checkIn}
            checkOut={item.checkOut}
            contact={item.contact}
            gallery={item.gallery}
            userRating={item.userRating}
            price={item.price}
            currency={item.currency}
        />
    );

    const [searchText, setSearchText] = useState('');
    const [isLoading, setLoading] = useState(true);
    const [filteredData, setFilteredData] = useState([]);
    const [masterData, setMasterData] = useState([]);
    const apiURL = 'https://run.mocky.io/v3/eef3c24d-5bfd-4881-9af7-0b404ce09507';
    const getData = async () => {
        try {
            const response = await fetch(apiURL);
            const json = await response.json();
            setMasterData(json);
            setFilteredData(json);
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        getData();
        return () => {
            setSearchText('');
            setFilteredData([]);
            setMasterData([]);
        };
    }, []);

    const search = () => {
        Keyboard.dismiss();
    }

    const sortBy = (value) => {
        switch (value) {
            case 'price-lowest-first': {
                setFilteredData([...filteredData].sort(function (a, b) {
                    return a.price - b.price;
                }));
                break;
            }
            case 'price-highest-first': {
                setFilteredData([...filteredData].sort(function (a, b) {
                    return b.price - a.price;
                }));
                break;
            }
            case 'stars': {
                setFilteredData([...filteredData].sort(function (a, b) {
                    return b.stars - a.stars;
                }));
                break;
            }
            case 'rating': {
                setFilteredData([...filteredData].sort(function (a, b) {
                    return b.userRating - a.userRating;
                }));
                break;
            }
        }
    }

    const searchInName = (value) => {
        if (value) {
            setFilteredData([...filteredData].filter(
                (item) => {
                    const itemData = item.name ? item.name.toUpperCase() : '';
                    const textData = value.toUpperCase();
                    return itemData.indexOf(textData) > -1;
                }).map((x) => x));
            setSearchText(value);
        } else {
            setSearchText(value);
            setFilteredData(masterData);
        }

    }
    if (isLoading) {
        return (
            <ActivityIndicator
                style={styles.activityIndicator}
                size={'large'}
                color={colors.accent}
            />
        );
    }
    return (
        <SafeAreaView style={{flex: 1}}>
            <CollapsibleHeaderFlatList
                CollapsibleHeaderComponent={
                    <View style={styles.searchBarContainer}>
                        <View style={styles.searchBarWrapper}>
                            <TextInput
                                style={styles.searchBar}
                                value={searchText}
                                underlineColorAndroid="transparent"
                                editable
                                maxLength={40}
                                placeholder={'Search'}
                                onChangeText={(text) => searchInName(text)}
                            />
                            <Pressable onPress={() => search()}>
                                <Ionicons name="md-search-outline" style={styles.searchIcon} size={20}
                                          color={colors.accent}/>
                            </Pressable>
                        </View>
                        <Text style={styles.constLabel}>Filter</Text>
                        <View style={styles.filterContainer}>
                            <TouchableOpacity onPress={() => sortBy('price-highest-first')}>
                                <FilterItem iconName={"md-trending-down-outline"} label={"Price"}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => sortBy('price-lowest-first')}>
                                <FilterItem iconName={"md-trending-up-outline"} label={"Price"}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => sortBy('stars')}>
                                <FilterItem iconName={"md-star-outline"} label={"Stars"}/>
                            </TouchableOpacity>
                        </View>
                    </View>

                }
                data={filteredData}
                renderItem={renderItem}
                showsVerticalScrollIndicator={false}
                keyExtractor={({id}) => id.toString()}
                headerHeight={200}
                statusBarHeight={Platform.OS === 'ios' ? 20 : 0}
            />

        </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    activityIndicator: {
        alignItems: 'center',
        flex: 1,
    },
    searchBarContainer: {
        backgroundColor: '#e8e8e8',
        padding: padding.standard,
    },
    constLabel: {
        padding: padding.standard,
        marginTop: margin.short,
    },
    searchBar: {
        height: 48,
        width: '100%',
        paddingHorizontal: padding.standard,
        backgroundColor: colors.background,
    },
    filterContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },
    searchBarWrapper: {
        flexDirection: 'row',
        backgroundColor: colors.background,
        paddingRight: 36,
        alignItems: 'center',
    },
    searchIcon: {
        paddingVertical: padding.short,
        paddingRight: padding.standard,
        alignSelf: 'center',
        justifyContent: 'center',
    },
});

export default Home;
