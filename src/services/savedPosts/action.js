import {deleteAPost, saveAPost} from "./constants";

export function savePost(item) {
    return {
        type: saveAPost,
        item,
    };
}

export function deletePost(item) {
    return {
        type: deleteAPost,
        item,
    };
}

