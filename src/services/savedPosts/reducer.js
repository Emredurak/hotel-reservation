import {deleteAPost, saveAPost} from "./constants";


const initialState = {
    savedPosts: []
};

const savedPostsReducer = (state = initialState, action) => {
    switch (action.type) {
        case saveAPost:
            return {
                ...state,
                savedPosts: [...state.savedPosts, action.item]
            }
        case deleteAPost:
            let index = state.savedPosts.indexOf(action.item)
            return {
                ...state,
                savedPosts: [
                    ...state.savedPosts.slice(0, index),
                    ...state.savedPosts.slice(index + 1)]
            }
        default:
            return state;
    }
};

export default savedPostsReducer;
