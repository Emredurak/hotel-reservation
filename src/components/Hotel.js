import React, {useState} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import getSymbolFromCurrency from "currency-symbol-map";
import {Ionicons} from '@expo/vector-icons';
import {Rating} from 'react-native-ratings';
import {colors} from "../../style/colors";
import {border, margin, padding} from "../../style/dimensions";
import {fonts} from "../../style/variables";

function Hotel(
    {
        navigation,
        id,
        name,
        location,
        stars,
        checkIn,
        checkOut,
        contact,
        gallery,
        userRating,
        price,
        currency
    }) {
    const initialImage = gallery ? gallery[0] : '';
    const [imageUri, setImageUri] = useState(initialImage);

    function imageLoadError() {
        //setImageUri(noImageAvailableUri);
    }

    return (
        <TouchableOpacity onPress={() => navigation.navigate('Detail', {
            id,
            name,
            location,
            stars,
            checkIn,
            checkOut,
            contact,
            gallery,
            userRating,
            price,
            currency
        })}>
            <View style={styles.container}>
                <Image source={{uri: imageUri}}
                       onError={() => (imageLoadError())}
                       style={[styles.coverImage]}/>
                <View style={styles.infoBox}>
                    <View style={styles.infoTextWrapper}>
                        <Text style={styles.name}>{name}</Text>
                        <View style={styles.location}>
                            <Ionicons name="md-location-outline" size={20} color={colors.textPrimary}/>
                            <Text style={styles.city}>{location.city}</Text>
                        </View>
                        <Rating
                            style={styles.rating}
                            type="custom"
                            readonly
                            startingValue={stars}
                            ratingBackgroundColor='#f1c40f30'
                            imageSize={16}/>
                        <Text style={styles.ratingText}>{userRating}</Text>
                    </View>

                    <Text style={styles.price}>{price} {getSymbolFromCurrency(currency)}</Text>

                </View>
                <Ionicons name="md-chevron-forward" style={styles.rightIcon} size={20} color={colors.textPrimary}/>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        marginVertical: margin.xShort,
        padding: padding.short,
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'white',
    },
    coverImage: {
        width: 100,
        height: 150,
        borderRadius: border.imageRadius,
    },
    infoBox: {
        flex: 1,
        flexGrow: 1,
        marginLeft: margin.short,
        backgroundColor: colors.background,
        padding: padding.xShort,
        alignItems: 'flex-start',
        justifyContent: 'space-between'
    },
    price: {
        backgroundColor: colors.background,
        height: 32,
        color: colors.priceBgColor,
        borderWidth: border.thin,
        borderRadius: border.imageRadius,
        borderColor: colors.priceBgColor,
        fontWeight: 'bold',
        alignSelf: 'flex-end',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: padding.standard,
        paddingVertical: padding.xShort,
    },
    infoTextWrapper: {
        flex: 1,
        alignItems: 'flex-start',
    },
    name: {
        fontFamily: fonts.standardFontFamily,
        fontWeight: 'bold',
    },
    location: {
        flexDirection: 'row',
    },
    city: {
        fontFamily: fonts.standardFontFamily,
        alignSelf: 'center',
    },
    rating: {
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: margin.short,
    },
    ratingText: {
        backgroundColor: colors.textPrimary,
        color: colors.background,
        borderRadius: 4,
        fontWeight: 'bold',
        padding: 2,
        alignSelf: 'flex-start',
    },
    rightIcon: {
        alignSelf: 'center',
        marginBottom: 20,
    },
});
export default Hotel;
