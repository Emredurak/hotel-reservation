import React from "react";
import {StyleSheet, Text, View} from "react-native";
import {Ionicons} from '@expo/vector-icons';
import {colors} from "../../style/colors";
import {border, margin, padding} from "../../style/dimensions";


const FilterItem = (props) => {

    return (
        <View style={styles.container}>
            <Ionicons name={props.iconName} style={styles.icon} size={20}/>
            <Text style={styles.label}>{props.label}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: padding.standard,
        paddingVertical: padding.short,
        marginHorizontal: margin.short,
        marginBottom: margin.short,
        backgroundColor: colors.buttonOutlineBgColor,
        borderRadius: border.bigRadius,
        alignItems: 'center',
    },
    label: {
        color: 'black',
        marginRight: margin.xShort,
        textAlignVertical: 'center',
    },
    icon: {
        marginRight: margin.xShort,
        color: colors.accent,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
});
export default FilterItem;
