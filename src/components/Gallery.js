import React, {useEffect, useRef, useState} from 'react';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import {Dimensions, Platform, StyleSheet, View} from 'react-native';
import {border, padding} from "../../style/dimensions";
import {colors} from "../../style/colors";

const {width: screenWidth} = Dimensions.get('window');

function Gallery({imageSource}) {
    const imageList = imageSource.map(x => {
        let obj = {}
        obj['title'] = " ";
        obj['subtitle'] = " ";
        obj['illustration'] = x;
        return obj
    });
    const [entries, setEntries] = useState([]);
    const carouselRef = useRef(null);
    useEffect(() => {
        setEntries(imageList);
    }, []);

    function handleImageError(url) {
        console.log("Image at '", url, "' couldn't loaded!");
    }

    function renderItem({item, index}, parallaxProps) {
        return (
            <View style={styles.item}>
                <ParallaxImage
                    source={{uri: item.illustration}}
                    onError={() => handleImageError(item.illustration)}
                    containerStyle={styles.imageContainer}
                    style={styles.image}
                    parallaxFactor={0.4}
                    {...parallaxProps}
                />
            </View>
        );
    }

    return (
        <View style={styles.carouselWrapper}>
            <Carousel
                ref={carouselRef}
                sliderWidth={screenWidth}
                sliderHeight={screenWidth}
                itemWidth={screenWidth / 3}
                data={entries}
                renderItem={renderItem}
                hasParallaxImages={true}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    carouselWrapper: {
        paddingVertical: padding.standard,
        backgroundColor: '#efefef',
        borderBottomLeftRadius: border.bigRadius,
        borderBottomRightRadius: border.bigRadius,
        elevation: 4,
    },
    item: {
        width: screenWidth / 3,
        height: screenWidth / 2,
    },
    container: {
        flex: 1,
        backgroundColor: colors.background,
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ios: 0, android: 1}), // Prevent a random Android rendering issue
        backgroundColor: '#FFF',
        borderRadius: 8,
    },
    image: {
        resizeMode: 'contain',
    },
})
export default Gallery;
