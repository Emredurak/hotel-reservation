import React from "react";
import {StyleSheet, Text, View} from "react-native";
import {Ionicons} from '@expo/vector-icons';
import {colors} from "../../style/colors";
import {margin, padding} from "../../style/dimensions";

const InfoElement = (props) => {

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Ionicons name={props.iconName} style={styles.icon} size={20} color={colors.textPrimary}/>
                <Text style={styles.label}>{props.label}</Text>
            </View>
            <View style={styles.innerElement}>
                {
                    props.children ? props.children :
                        <Text>{props.text}</Text>
                }
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginVertical: margin.xShort,
        flex: 1,
        backgroundColor: 'white',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    label: {
        paddingLeft: padding.xShort,
        paddingRight: padding.standard,
        color: colors.textPrimary,
        textAlign: 'left',
        textAlignVertical: 'center',
        borderBottomWidth: 1,
        borderColor: colors.textSecondary,

    },
    icon: {
        marginRight: margin.xShort,
        color: colors.accent,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    innerElement: {
        marginHorizontal: margin.xShort,
        padding: padding.standard,
    }
});
export default InfoElement;
