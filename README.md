# Readme

## This is a sample react-native project for hotel booking application.

#### Clone this repository

> git clone https://gitlab.com/Emredurak/hotel-reservation.git

#### Installation

After cloning the project, go into 'hotel-reservation folder' and run:
> cd hotel-reservation

Assuming that you have Node 12 LTS or greater installed, you can use npm to install the Expo CLI command line utility:
> npm install -g expo-cli

Install the node_modules by typing:
> npm install


Start application by using:
> expo start

## Project Description

This project developed via React Native Expo. Application consist of two main pages. One of them is Home Page which
includes a list view to show hotels. Second one is detail page for the selected hotel. Due to image quality constraints
small image views preferred.

### Basic Libraries Used In Project

- React navigation
- React-native-collapsible-header-views
- Redux
- Redux-persist
- React-native-snap-carousel

### Contact

If you have any question or suggestion feel free to contact me!

https://linkedin.com/in/emredurak4316
