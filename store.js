import {combineReducers, createStore} from 'redux';


import {persistReducer, persistStore} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import savedPostsReducer from "./src/services/savedPosts/reducer";

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const rootReducer = combineReducers({
  savedPosts: persistReducer(persistConfig, savedPostsReducer),
});

const configureStore = () => {
  const store = createStore(rootReducer);
  let persistor = persistStore(store);

  return {persistor, store};
};

export default configureStore;
