import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {Provider} from 'react-redux';
import configureStore from './store';
import {PersistGate} from 'redux-persist/integration/react';
import Home from "./src/screens/Home";
import Detail from "./src/screens/Detail";
import {colors} from "./style/colors";

const Stack = createStackNavigator();
const {store, persistor} = configureStore();

function App() {
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <NavigationContainer>
                    <Stack.Navigator
                        initialRouteName={'Home'}
                        screenOptions={{
                            ...TransitionPresets.SlideFromRightIOS,
                            gestureEnabled: false,
                            headerTitleAlign: 'center',
                            headerTitleStyle: {
                                color: colors.textPrimary,
                                textTransform: 'uppercase',
                            },
                        }}>
                        <Stack.Screen name="Home" component={Home}/>
                        <Stack.Screen name="Detail" component={Detail}/>
                    </Stack.Navigator>
                </NavigationContainer>
            </PersistGate>
        </Provider>
    );
}

export default App;
