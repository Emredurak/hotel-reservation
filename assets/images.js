import {Image} from 'react-native';
import noImage from "./noImageAvailable.png";

export const noImageAvailableUri = Image.resolveAssetSource(noImage).uri;
